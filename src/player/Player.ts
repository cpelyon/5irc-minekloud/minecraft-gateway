export enum PlayerStatus {
  connected = 'connected',
  disconnected = 'disconnected',
}

export class Player {
  username: string
  status: PlayerStatus

  constructor(username: string) {
    this.username = username
    this.status = PlayerStatus.disconnected
  }

  setConnected() {
    this.status = PlayerStatus.connected
  }

  setDisconnected() {
    this.status = PlayerStatus.disconnected
  }
}
